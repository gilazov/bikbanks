package com.gilazov.data.db;

import com.gilazov.data.common.CacheManager;
import com.gilazov.data.db.table.BanksTable;
import com.gilazov.data.entity.object.BankDAO;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.queries.Query;
import com.pushtorefresh.storio.sqlite.queries.RawQuery;

import java.util.List;

import rx.Observable;

/**
 * Created by r on 25.10.16.
 */

public class BanksCacheManager implements CacheManager<BankDAO>{

    private StorIOSQLite db;

    public BanksCacheManager(StorIOSQLite db) {
        this.db = db;
    }

    public void save(List<BankDAO> bankDAOs) {
        db
                .put()
                .objects(bankDAOs)
                .prepare()
                .asRxObservable()
                .subscribe();
    }

    public void save(BankDAO bankDAO) {
        db
                .put()
                .object(bankDAO)
                .prepare()
                .asRxObservable()
                .subscribe();
    }

    public void delete(List<BankDAO> bankDAOs) {
        db
                .delete()
                .objects(bankDAOs)
                .prepare()
                .asRxObservable()
                .subscribe();
    }

    public void delete(BankDAO bankDAO) {
        db
                .delete()
                .object(bankDAO)
                .prepare()
                .asRxObservable()
                .subscribe();
    }

    public Observable<List<BankDAO>> getAll() {
        return db
                .get()
                .listOfObjects(BankDAO.class)
                .withQuery(Query.builder()
                        .table(BanksTable.TABLE)
                        .build())
                .prepare()
                .asRxObservable();
    }

    @Override
    public void clear() {
        db
                .executeSQL()
                .withQuery(RawQuery.builder()
                            .query("delete from " + BanksTable.TABLE)
                            .build())
                .prepare()
                .asRxObservable()
                .subscribe();
    }

}
