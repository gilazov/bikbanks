package com.gilazov.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.gilazov.data.db.table.BanksTable;

/**
 * Created by r on 24.10.16.
 */

public class DBOpenHelper extends SQLiteOpenHelper{

    public DBOpenHelper(Context context){
        super(context, DB.DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(BanksTable.getCreateTableQuery());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
