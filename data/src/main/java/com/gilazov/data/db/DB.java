package com.gilazov.data.db;

import android.content.Context;

import com.gilazov.data.entity.object.BankDAO;
import com.gilazov.data.entity.object.BankDAOStorIOSQLiteDeleteResolver;
import com.gilazov.data.entity.object.BankDAOStorIOSQLiteGetResolver;
import com.gilazov.data.entity.object.BankDAOStorIOSQLitePutResolver;
import com.pushtorefresh.storio.sqlite.SQLiteTypeMapping;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite;

/**
 * Created by r on 24.10.16.
 */

public class DB {

    public static String DB_NAME = "bikbanks_db";

    private static StorIOSQLite DB;

    public static synchronized StorIOSQLite getInstance(Context context) {

        if (DB == null) {
            DB =  DefaultStorIOSQLite.builder()
                    .sqliteOpenHelper(new DBOpenHelper(context))
                    .addTypeMapping(BankDAO.class, SQLiteTypeMapping.<BankDAO>builder()
                            .putResolver(new BankDAOStorIOSQLitePutResolver()) // object that knows how to perform Put Operation (insert or update)
                            .getResolver(new BankDAOStorIOSQLiteGetResolver()) // object that knows how to perform Get Operation
                            .deleteResolver(new BankDAOStorIOSQLiteDeleteResolver())  // object that knows how to perform Delete Operation
                            .build())
                    .build();
        }

        return DB;
    }

}
