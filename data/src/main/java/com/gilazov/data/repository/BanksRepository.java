package com.gilazov.data.repository;

import com.gilazov.data.common.CacheManager;
import com.gilazov.data.entity.object.BankDAO;
import com.gilazov.data.entity.response.GetBanksResponse;
import com.gilazov.data.network.BanksService;

import java.util.List;

import rx.Observable;

/**
 * Created by r on 24.10.16.
 *
 */

public class BanksRepository {

    private CacheManager<BankDAO> cache;

    private BanksService banksService;

    public BanksRepository(CacheManager<BankDAO> cache, BanksService banksService) {
        this.cache = cache;
        this.banksService = banksService;
    }

    protected Observable<List<BankDAO>> getDataFromNetwork() {
       return banksService.getBanks()
               .map(GetBanksResponse::getBanks);
    }

    protected Observable<List<BankDAO>> getDataFromCache() {
        return cache.getAll();
    }

    public Observable<List<BankDAO>> getAll(){
        return getDataFromNetwork();
    }

}
