package com.gilazov.data.entity.object;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.gilazov.data.db.table.BanksTable;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import org.simpleframework.xml.Element;

@Element(name = "Record")
@StorIOSQLiteType(table = BanksTable.TABLE)
public class BankDAO {

    @Nullable
    @StorIOSQLiteColumn(name = BanksTable.COLUMN_ID, key = true)
    Long id;

    @NonNull
    @Element(name = "ShortName")
    @StorIOSQLiteColumn(name = BanksTable.COLUMN_NAME)
    String name;

    @NonNull
    @Element(name = "Bic")
    @StorIOSQLiteColumn(name = BanksTable.COLUMN_BIK)
    int bik;

    public BankDAO() {
    }

    @Nullable
    public Long getId() {
        return id;
    }

    public void setId(@Nullable Long id) {
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public int getBik() {
        return bik;
    }

    public void setBik(@NonNull int bik) {
        this.bik = bik;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BankDAO bankDAO = (BankDAO) o;

        if (bik != bankDAO.bik) return false;
        if (id != null ? !id.equals(bankDAO.id) : bankDAO.id != null) return false;
        return name.equals(bankDAO.name);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + name.hashCode();
        result = 31 * result + bik;
        return result;
    }
}
