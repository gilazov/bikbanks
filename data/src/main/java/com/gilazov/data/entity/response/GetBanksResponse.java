package com.gilazov.data.entity.response;

import com.gilazov.data.entity.object.BankDAO;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by r on 18.11.16.
 */
@Root(name = "BicCode")
public class GetBanksResponse {

    public GetBanksResponse() {
    }

    @ElementList(entry="Record", inline=true)
    public List<BankDAO> banks;

    public List<BankDAO> getBanks() {
        return banks;
    }

    public void setBanks(List<BankDAO> banks) {
        this.banks = banks;
    }
}
