package com.gilazov.data.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Retrofit client configuration
 */

public class CBApi {

    public static final String BASE_URL = "http://www.cbr.ru/scripts/";

    public static BanksService getBanksService(){
        return getRetrofit().create(BanksService.class);
    }

    private static Retrofit getRetrofit(){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

       /* Interceptor win1251Interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .addHeader("Content-Type", "text/plain; charset=WINDOWS-1251")
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        };*/

        OkHttpClient okClient = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    //.addInterceptor(win1251Interceptor)
                    .connectTimeout(5000, TimeUnit.MILLISECONDS)
                    .build();

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okClient)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
    }

}
