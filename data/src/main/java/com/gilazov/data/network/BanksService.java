package com.gilazov.data.network;

import com.gilazov.data.entity.response.GetBanksResponse;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by ruslan on 19.07.16.
 */
public interface BanksService {

    @GET("XML_bic.asp")
    Observable<GetBanksResponse> getBanks();

}
