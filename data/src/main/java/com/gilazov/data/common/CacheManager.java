package com.gilazov.data.common;

import java.util.List;

import rx.Observable;

/**
 * Created by r on 25.10.16.
 */

public interface CacheManager<T> {

    void save(List<T> entities);
    void save(T entity);
    void delete(List<T> entities);
    void delete(T entity);
    Observable<List<T>> getAll();
    void clear();

}
