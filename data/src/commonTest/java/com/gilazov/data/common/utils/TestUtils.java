package com.gilazov.data.common.utils;

import java.lang.reflect.Field;

/**
 * Created by r on 13.11.16.
 */

public class TestUtils {

    public static void resetSingleton(Class clazz, String fieldName) {
        Field instance;
        try {
            instance = clazz.getDeclaredField(fieldName);
            instance.setAccessible(true);
            instance.set(null, null);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

}
