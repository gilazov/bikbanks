package com.gilazov.data.common;

import com.gilazov.data.entity.object.BankDAO;

import java.util.ArrayList;
import java.util.List;

import static android.R.attr.id;

public class TestDataFactory {

    public static List<BankDAO> makeListBankDAOs(int count){
        List<BankDAO> list = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            list.add(makeBankDAO(i));
        }
        return list;
    }

    public static BankDAO makeBankDAO(int unicId){
        BankDAO bankDAO = new BankDAO();
        bankDAO.setName("name "+ id);
        bankDAO.setBik(id);
        return bankDAO;
    }

}
