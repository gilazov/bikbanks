package com.gilazov.data.db;

import com.gilazov.data.common.ImmediateSchedulersTestRule;
import com.gilazov.data.common.TestDataFactory;
import com.gilazov.data.common.utils.TestUtils;
import com.gilazov.data.entity.object.BankDAO;
import com.pushtorefresh.storio.sqlite.BuildConfig;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.Arrays;
import java.util.List;

import rx.observers.TestSubscriber;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class BanksCacheManagerTest {

    BanksCacheManager cacheManager;

    @Rule
    public ImmediateSchedulersTestRule schedulersTestRule = new ImmediateSchedulersTestRule();

    @Before
    public void setup(){
        cacheManager = new BanksCacheManager(DB.getInstance(RuntimeEnvironment.application));
    }

    @After
    public void resetPhotoCacheManager()throws SecurityException, NoSuchFieldException,
            IllegalArgumentException, IllegalAccessException{

            TestUtils.resetSingleton(DB.class, "DB");
    }

    @Test
    public void getAll(){
        BankDAO photoDAO = TestDataFactory.makeBankDAO(1);
        BankDAO photoDAO2 = TestDataFactory.makeBankDAO(2);
        List<BankDAO> photoDAOs = Arrays.asList(photoDAO, photoDAO2);

        cacheManager.save(photoDAOs);

        TestSubscriber<List<BankDAO>> result = new TestSubscriber<>();
        cacheManager.getAll().subscribe(result);

        result.assertNoErrors();
        result.assertValue(photoDAOs);

    }

    @Test
    public void save(){
        BankDAO expectedPhoto = TestDataFactory.makeBankDAO(1);

        cacheManager.save(expectedPhoto);

        TestSubscriber<List<BankDAO>> result = new TestSubscriber<>();
        cacheManager.getAll().subscribe(result);

        result.assertNoErrors();
        assertThat(result.getOnNextEvents().get(0).contains(expectedPhoto)).isTrue();

    }

    @Test
    public void delete(){
        BankDAO photo = TestDataFactory.makeBankDAO(1);
        cacheManager.save(photo);

        cacheManager.delete(photo);

        TestSubscriber<List<BankDAO>> result = new TestSubscriber<>();
        cacheManager.getAll().subscribe(result);

        result.assertNoErrors();
        assertThat(result.getOnNextEvents().get(0).contains(photo)).isFalse();

    }

    @Test
   public void clear(){
        List<BankDAO> photos = TestDataFactory.makeListBankDAOs(10);
        cacheManager.save(photos);

        cacheManager.clear();

        TestSubscriber<List<BankDAO>> result = new TestSubscriber<>();
        cacheManager.getAll().subscribe(result);

        result.assertNoErrors();
        assertThat(result.getOnNextEvents().get(0).size()).isEqualTo(0);
    }

}