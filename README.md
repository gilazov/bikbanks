# README #

App for showing list of banks with bic form cbr.ru, and bank details from htmlweb

http://www.cbr.ru/scripts/XML_bic.asp

https://htmlweb.ru/service/bank.php


### Intro ###

* Clean Architecture
* MVP
* RxJava
* REtorfit (SimpleXmlParser for CBR and gson for bank details )
* storeio (sql wrapper ) as local db

### Bugs ###

* banks list not draw, but will draw after power button click, or change app (ui lugs and give unexpected behavior (showing progress bars, or error view although error didnt occur) )
* encoding parsing (cbr contains only win-1251)