package com.gilazov.domain.usecase;

import com.gilazov.data.entity.object.BankDAO;
import com.gilazov.data.repository.BanksRepository;
import com.gilazov.domain.model.Bank;

import java.util.List;

import rx.Observable;

/**
 * Created by r on 24.10.16.
 */

public class GetBanksUseCase {

    BanksRepository repository;

    public GetBanksUseCase(BanksRepository repository) {
        this.repository = repository;
    }

    public Observable<List<Bank>> getBanks(){
        return repository
                .getAll()
                .flatMap(Observable::from)
                .map(this::toBanks)
                .toList();
    }

    private Bank toBanks (BankDAO bankDAO){

        Bank bank = new Bank();
        bank.setName(bankDAO.getName());
        bank.setBik(bankDAO.getBik());

        return bank;
    }
}
