package com.gilazov.domain.model;

/**
 * Created by r on 24.10.16.
 */

public class Bank {
    private String name;

    public int getBik() {
        return bik;
    }

    public void setBik(int bik) {
        this.bik = bik;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private int bik;
}
