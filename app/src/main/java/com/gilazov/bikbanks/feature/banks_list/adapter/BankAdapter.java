package com.gilazov.bikbanks.feature.banks_list.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gilazov.bikbanks.feature.banks_list.viewholder.BankViewHolder;
import com.gilazov.domain.model.Bank;
import com.gilazov.bikbanks.R;
import com.gilazov.bikbanks.base.adapter.BaseRecyclerAdapter;

import java.util.List;

/**
 * Created by ruslan on 12.09.16.
 */
public class BankAdapter extends BaseRecyclerAdapter<Bank, BankViewHolder> {

    public BankAdapter(List<Bank> photos) {
            super(photos);
            }

    @Override
    public BankViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.fragment_banks_item, parent, false);
            return new BankViewHolder(view, mOnItemClickListener);
            }

    @Override
    public void onBindViewHolder(BankViewHolder holder, int position) {
            holder.init(getItem(position));
            }

}
