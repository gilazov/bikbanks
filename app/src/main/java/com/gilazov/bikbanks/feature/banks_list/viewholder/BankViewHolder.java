package com.gilazov.bikbanks.feature.banks_list.viewholder;

import android.view.View;
import android.widget.TextView;

import com.gilazov.bikbanks.R;
import com.gilazov.bikbanks.base.adapter.listener.OnItemClickListener;
import com.gilazov.bikbanks.base.adapter.viewholder.BaseViewHolder;
import com.gilazov.domain.model.Bank;

import butterknife.BindView;

/**
 * Created by ruslan on 12.09.16.
 */

public class BankViewHolder extends BaseViewHolder {

    @BindView(R.id.bankName)
    public TextView bankName;
    @BindView(R.id.bankBik)
    public TextView bankBik;

    public BankViewHolder(View itemView, OnItemClickListener clickListener) {
        super(itemView, clickListener);
    }

    public void init(Bank bank) {
        bankName.setText(bank.getName());
        bankBik.setText(bank.getBik());
    }

}
