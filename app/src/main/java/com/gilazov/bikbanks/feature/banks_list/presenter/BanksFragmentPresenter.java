package com.gilazov.bikbanks.feature.banks_list.presenter;

import com.gilazov.data.db.BanksCacheManager;
import com.gilazov.data.db.DB;
import com.gilazov.data.network.CBApi;
import com.gilazov.data.repository.BanksRepository;
import com.gilazov.domain.model.Bank;
import com.gilazov.domain.usecase.GetBanksUseCase;
import com.gilazov.bikbanks.base.presenter.MVPRecyclerFragmentPresenter;
import com.gilazov.bikbanks.base.view.MVPRecyclerFragmentView;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by r on 07.11.16.
 */

public class BanksFragmentPresenter implements MVPRecyclerFragmentPresenter<Bank>{

    private CompositeSubscription mCompositeSubscription  = new CompositeSubscription();

    private MVPRecyclerFragmentView view;

    private GetBanksUseCase getBanksUseCase;

    public void setView(MVPRecyclerFragmentView view) {
        this.view = view;
    }

    public void setModel(GetBanksUseCase getPhotosUseCase) {
        this.getBanksUseCase = getPhotosUseCase;
    }

    public BanksFragmentPresenter(MVPRecyclerFragmentView view) {
        setView(view);

        getBanksUseCase = new GetBanksUseCase(new BanksRepository(new BanksCacheManager(DB.getInstance(view.getContext())), CBApi.getBanksService()));

        setModel(getBanksUseCase);
    }

    public BanksFragmentPresenter(MVPRecyclerFragmentView view, GetBanksUseCase useCase) {
        setView(view);

        setModel(useCase);
    }

    @Override
    public void loadData() {

        view.showProgress();//todo fix with rx
        mCompositeSubscription.add(
                getBanksUseCase
                    .getBanks()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(banks -> {
                        if (view != null) {
                            //todo fix with switchIfEmpty
                            if (banks.size() == 0) {
                                view.showEmptyView();
                            } else {
                                view.setItems(banks);
                                view.hideProgress();
                            }
                        }
                    }, error ->  {if (view != null) {
                        view.showErrorView();
                    }
        }));

    }

    @Override
    public void destroy() {
        view = null;
        mCompositeSubscription.unsubscribe();
    }

}
