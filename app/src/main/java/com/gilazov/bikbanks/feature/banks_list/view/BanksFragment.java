package com.gilazov.bikbanks.feature.banks_list.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.gilazov.bikbanks.R;
import com.gilazov.bikbanks.base.fragment.BaseRecyclerFragment;
import com.gilazov.bikbanks.base.view.MVPRecyclerFragmentView;
import com.gilazov.bikbanks.feature.banks_list.adapter.BankAdapter;
import com.gilazov.bikbanks.feature.banks_list.presenter.BanksFragmentPresenter;
import com.gilazov.bikbanks.feature.banks_list.viewholder.BankViewHolder;
import com.gilazov.domain.model.Bank;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by ruslan on 12.09.16.
 */
public class BanksFragment extends BaseRecyclerFragment<Bank,BankViewHolder> implements MVPRecyclerFragmentView<Bank>{

    private BanksFragmentPresenter presenter;

    @SuppressWarnings("unused")
    public static BanksFragment newInstance() {
        BanksFragment fragment = new BanksFragment();
        return fragment;
    }

    @Override
    protected boolean isNeedDivider() {
        return true;
    }

    @Override
    protected void onRecyclerViewItemClick(View view, int position) {
        final Bank bank = getAdapter().getItem(position);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_recycler;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        Context context = view.getContext();

        presenter = new BanksFragmentPresenter(this);

        presenter.loadData();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.destroy();
    }

    @Override
    public void setItems(List<Bank> list) {
        setAdapter(new BankAdapter(list));
    }

    @Override
    public void showProgress() {
        showProgress(true);
        hideEmptyView();
        hideErrorView();
    }

    @Override
    public void hideProgress() {
        showProgress(false);
    }

    @Override
    public void showEmptyView() {
        showEmptyView(true);
        hideProgress();
        hideErrorView();
    }

    @Override
    public void hideEmptyView() {
        showEmptyView(false);
    }

    @Override
    public void showErrorView() {
        showErrorView(true);
        hideProgress();
        hideEmptyView();
    }

    @Override
    public void hideErrorView() {
        showErrorView(false);
    }

}