package com.gilazov.bikbanks.base.adapter.listener;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(View view, int position);
}
