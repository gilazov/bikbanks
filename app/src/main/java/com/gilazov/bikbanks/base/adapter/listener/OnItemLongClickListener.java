package com.gilazov.bikbanks.base.adapter.listener;

import android.view.View;

public interface OnItemLongClickListener {
    boolean onItemLongClick(View view, int position);
}