package com.gilazov.bikbanks.base.presenter;

/**
 * Created by r on 07.11.16.
 */

public interface MVPRecyclerFragmentPresenter<T> {

    void loadData();
    void destroy();

}
