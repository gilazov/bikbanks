package com.gilazov.bikbanks.test.common;

import com.gilazov.domain.model.Bank;

import java.util.ArrayList;
import java.util.List;

public class TestDataFactory {

    public static List<Bank> makeListPhotos(int count){
        List<Bank> list = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            list.add(makePhoto(i));
        }
        return list;
    }

    public static Bank makePhoto(int id){
        Bank photo = new Bank();
        photo.setName("name "+ id);
        photo.setId(id);
        photo.setUrl("url/"+ id);
        return photo;
    }

}
